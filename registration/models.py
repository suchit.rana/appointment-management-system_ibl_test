from django.db import models
from djchoices import DjangoChoices, ChoiceItem


class users(models.Model):
    class TYPE_CHOICES(DjangoChoices):
        customer = ChoiceItem(0, 'Customer')
        advisor = ChoiceItem(1, 'Advisor')


    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, default=None, blank=True, null=True)
    type_id = models.IntegerField(choices=TYPE_CHOICES.choices, default=None, blank=True, null=True)
    email = models.EmailField(max_length=255, default=None, blank=True, null=True)
    gender = models.IntegerField(default=None, blank=True, null=True)
    password = models.CharField(max_length=255, default=None, blank=True, null=True)
    mobile_no = models.CharField(max_length=20, default=None, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_type_name(self):
        type = self.TYPE_CHOICES.choices
        results = [value for key, value in type if key == self.type_id]
        if not results:
            results = ""
        else:
            results = results[0]
        return results


    def __str__(self):
        return self.name

    @property
    def getUserName(self, id):
        return users.objects.values('name').get(id=id)