from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse
from .models import users as User
from django.contrib.auth.hashers import make_password
from datetime import datetime
from django.db.models import Q

# Create your views here.

def index(request):
    userdata=User.objects.all()
    context={'userdata':userdata}
    return render(request, 'user/userlist.html',context)

def adduser(request):
    responsedata = {}
    type_id=User._meta.get_field('type_id').choices
    context={'type_id':type_id}
    if request.method == 'POST':
        name = request.POST.get('name')
        type_id = int(request.POST.get('type_id'))
        email = request.POST.get('email')
        gender = request.POST.get('gender')
        password = request.POST.get('password')
        if password != "" and password != None:
            password=make_password(password)
        mobile_no = request.POST.get('mobile_no')
        try:
            checkunique=User.objects.filter(Q(name=name) | Q(email=email)).first()
            if checkunique is not None:
                responsedata['status'] = "error"
                responsedata['message'] = "This Email Or Name Are Already Registered !"
                return JsonResponse(responsedata)
            userssubmit = User.objects.create(name=name,type_id=type_id,email=email,gender=gender,password=password
                                              ,mobile_no=mobile_no)
            responsedata['status'] = "success"
            responsedata['message'] = "New User Added Successfully..!"
        except Exception as  e:
            responsedata['status'] = "error"
            responsedata['message'] = "SomeThing Wrong . Please Try Again !"

        return JsonResponse(responsedata)
    return render(request, 'user/adduser.html',context)

def edituser(request,id=None):
    responsedata = {}
    if request.method == 'POST':
        try:
            userssubmit = User.objects.get(id=request.POST.get('id'))
            userssubmit.name=request.POST.get('name')
            userssubmit.mobile_no = request.POST.get('mobile_no')
            userssubmit.email = request.POST.get('email')
            userssubmit.gender = int(request.POST.get('gender'))
            if request.POST.get('password') is not '':
                userssubmit.password=make_password(request.POST.get('password'))
            checkunique = User.objects.filter(Q(name=userssubmit.name) | Q(email=userssubmit.email)).exclude(id=request.POST.get('id')).first()
            if checkunique is not None:
                responsedata['status'] = "error"
                responsedata['message'] = "This Email Already Registered !"
                return JsonResponse(responsedata)
            userssubmit.save()
            responsedata['status'] = "success"
            responsedata['message'] = "User Updated Successfully..!"
        except Exception as  e:
            responsedata['status'] = "error"
            responsedata['message'] = "SomeThing Wrong . Please Try Again !"

        return JsonResponse(responsedata)
    try:
        updatedata=User.objects.get(id=id)
    except Exception as  e:
        response = redirect('/registration')
        return response

    type_id = int(updatedata.type_id)
    department = User.TYPE_CHOICES.choices
    response = HttpResponse()
    context = {'updatedata': updatedata,'type_id': type_id}
    return render(request, 'user/edituser.html',context)


