from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns =[
    path("", views.index),
    path("add", views.adduser, name="add"),
    path("edit/<int:id>", views.edituser, name="edit"),
    path("edit", views.edituser, name="edit"),

    ]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)