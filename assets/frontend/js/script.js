$('#advertise-form').validate({

    rules: {

        category: {
            required: true,
        },
        title: {
            required: true,
        },
        mobilen: {
            required: true,
        },
        address: {
            required: true,
        }
    },

    highlight: function(element) {

        $(element).attr("style", 'border-bottom: 2px solid red;');
        if (element.name == 'std') {}
    },
    unhighlight: function(element) {
        $(element).removeAttr("style");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        //Form data
        var form_data = $('#advertise-form').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="advertiseimage"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            data.append("advertiseimage", file_data[i]);
        }
        $.ajax({
            type: 'post',
            url: base_url + 'user/insertadvertise',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number]").val("");
                    setTimeout(function() {
                        window.location.replace(base_url + 'user/profile');
                    }, 2000);
                    alertify.success(data.message);
                } else {
                    alertify.error(data.message);
                }
            }
        });
    }
});


function editadvertise(id) {

    $.ajax({
        type: 'post',
        url: base_url + 'user/getadvertise',
        data: { id: id },
        success: function(response) {
            var data = jQuery.parseJSON(response);

            if (data.status == 'success') {
                var img = $("<img />");
                img.attr("style", "border:1px solid #ddd;");
                img.attr("src", base_url + 'media/memberads/' + data.message['image']);
                $('#dvPreview1').html(img);
                $('#category').val(data.message['adc_id']);
                $('#ads_id').val(data.message['ads_id']);
                $('.adtitle').val(data.message['titles']);
                $('#admobile').val(data.message['mobile']);
                $('.adaddress').val(data.message['address']);
            } else {}
        }
    });

}


function editsponsor(id) {

    $.ajax({
        type: 'post',
        url: base_url + 'user/getsponsor',
        data: { id: id },
        success: function(response) {
            var data = jQuery.parseJSON(response);

            if (data.status == 'success') {
                $('#s_id').val(data.message['s_id']);
                $.each(data.message['sponsor_type'].split(","), function(i, e) {
                    $("#spcategory option[value='" + e + "']").attr("selected", true);
                });
                /*$('#spcategory').val(data.message['titles']);*/
                $('#spamount').val(data.message['amount']);
                $('#spno').val(data.message['total_ads']);
            } else {}
        }
    });

}

$('#sponsor-form').validate({

    rules: {

        spcategory: {
            required: true,
        },
        amount: {
            required: true,
        },
        note: {
            required: true,
        }
    },

    highlight: function(element) {

        $(element).attr("style", 'border-bottom: 2px solid red;');
    },
    unhighlight: function(element) {
        $(element).removeAttr("style");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        //Form data
        var form_data = $('#sponsor-form').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: base_url + 'user/insertsponsor',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number]").val("");
                    setTimeout(function() {
                        window.location.replace(base_url + 'user/profile');
                    }, 2000);
                    alertify.success(data.message);
                } else {
                    alertify.error(data.message);
                }
            }
        });
    }
});

$(function() {
    $('#foobar input[type=radio]').change(function() {
        if ($(this).val() == 1) {
            $('#textboxname').removeClass('hide');
            $('.dname').val('');
        } else {
            $('#textboxname').addClass('hide');
            $('.dname').val('');
        }
    })
})
$(function() {
    $('#foobar1 input[type=radio]').change(function() {
        if ($(this).val() == 1) {
            $('#textboxstudentname').removeClass('hide');
            $('.sfname').val('');
        } else {
            $('#textboxstudentname').addClass('hide');
            $('.sfname').val('');
        }
    })
})


$('#donor-form').validate({

    rules: {
        amount: {
            required: true,
        },
        note: {
            required: true,
        }
    },

    highlight: function(element) {

        $(element).attr("style", 'border-bottom: 2px solid red;');
    },
    unhighlight: function(element) {
        $(element).removeAttr("style");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        //Form data
        var form_data = $('#donor-form').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: base_url + 'user/insertdonor',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number]").val("");
                    setTimeout(function() {
                        window.location.replace(base_url + 'user/profile');
                    }, 2000);
                    alertify.success(data.message);
                } else {
                    alertify.error(data.message);
                    $('#donormsg').html('<div id="" class="massage alert alert-success" style="" >' + data.message + '</div>');
                }
            }
        });
    }
});


function printdiv(id) {

    var newstr = document.all.item("bill" + id).innerHTML;
    var oldstr = document.body.innerHTML;
    document.body.innerHTML = newstr;
    window.print();
    document.body.innerHTML = oldstr;
    return false;
}

function editdonor(id) {

    $.ajax({
        type: 'post',
        url: base_url + 'user/getdonor',
        data: { id: id },
        success: function(response) {
            var data = jQuery.parseJSON(response);

            if (data.status == 'success') {
                $('#d_id').val(data.message['d_id']);
                if (data.message['do_name']) {
                    $('#textboxname').removeClass('hide');
                    $('.dname').val(data.message['do_name']);
                    $('input:radio[name=donor]')[1].checked = true;
                }
                $('#damount').val(data.message['amount']);
                $('.dnote').val(data.message['notee']);
            } else {}
        }
    });

}



function editstudent(id) {

    $.ajax({
        type: 'post',
        url: base_url + 'user/getstudent',
        data: { id: id },
        success: function(response) {
            var data = jQuery.parseJSON(response);

            if (data.status == 'success') {
                var img = $("<img />");
                img.attr("style", "border:1px solid #ddd;");
                img.attr("src", base_url + 'media/student/' + data.message['st_photo']);
                $('#dvPreview').html(img);
                $('#st_id').val(data.message['stu_id']);
                $('.sname').val(data.message['st_name']);
                $('#sdob').val(data.message['dob']);
                if (data.message['st_fname']) {
                    $('#textboxstudentname').removeClass('hide');
                    $('.sfname').val(data.message['st_fname']);
                    $('input:radio[name=studentfname]')[1].checked = true;
                }

                $('input:radio[name=gender]')[1].checked = true;

                $('.scname').val(data.message['school_name']);
                $('#sstd').val(data.message['std_id']);

            } else {}
        }
    });

}

$(function() {
    $("#upfile").change(function() {
        if (typeof(FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function() {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var img = $("<img />");
                        img.attr("style", "border:1px solid #ddd;");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});

$(function() {
    $("#upfile1").change(function() {
        /*  console.log($(this));*/
        if (typeof(FileReader) != "undefined") {
            var dvPrevie = $("#dvPreview1");
            dvPrevie.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function() {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var img = $("<img  style='border:1px solid #ddd;' src='" + e.target.result + "' />");

                        dvPrevie.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});

function clearimg() {
    var $el = $('#upfile');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    $('#dvPreview').html('<h4>Image</h4>');

}

function clearimg1() {
    var $el = $('#upfile1');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
    $('#dvPreview1').html('<h4>Image</h4>');

}
$('#pdf-form').validate({

    rules: {
        surnamesel: {
            required: true,
        }
    },

    highlight: function(element) {

        $(element).attr("style", 'border-bottom: 2px solid red;');
    },
    unhighlight: function(element) {
        $(element).removeAttr("style");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        //Form data
        var form_data = $('#pdf-form').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: base_url + 'user/surnamepdf1',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                $("input[type=text], textarea,input[type=email],input[type=password],input[type=number]").val("");
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    setTimeout(function() {
                        window.location.replace(base_url + 'user/profile');
                    }, 2000);
                    alertify.success(data.message);
                } else {
                    alertify.error(data.message);
                }
            }
        });
    }
});