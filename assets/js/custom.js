function notifictaion(title, message, type) {
    new PNotify({
        title: title,
        text: message,
        type: type,
        cornerclass: 'ui-pnotify-sharp'
    });
}

/*Change password*/
$('#changepass').validate({

    rules: {
        currentpass: {
            required: true,
            minlength: 6,
            maxlength: 18
        },
        newpass: {
            required: true,
            minlength: 6,
            maxlength: 18
        },
        conpass: {
            required: true,
            minlength: 6,
            maxlength: 18,
            equalTo: "#new"
        }
    },

    highlight: function (element) {
        $(element).addClass("field-error");
    },
    unhighlight: function (element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function (error, element) {
        return false;

    },
    submitHandler: function (form) {

        var data = new FormData();

        //Form data
        var form_data = $('#changepass').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/changepass',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function (data) {

                if (data.status == true) {
                    $('.msg').html('<div id="" class="massage alert alert-success" style="" >' + data.message + '</div>');
                    setTimeout(function () {
                        window.location.replace('/logout');
                    }, 1500);
                } else {
                    $('.msg').html('<div id="" class="massage alert alert-danger" style="" >' + data.message + '</div>');
                }
            }
        });
    }
});
/*End of change password*/

/*Admin Setting*/
$('#useradd').validate({
    rules: {
        name: {
            required: true
        },

        mobile_no: {
            required: true
        },
        email: {
            required: true
        },
        gender: {
            required: true
        },
        password: {
            required: true,
            minlength: 6,
            maxlength: 18
        },
        cpassword: {
            required: true,
            minlength: 6,
            maxlength: 18,
            equalTo: "#password"
        }
    },
    highlight: function (element) {
        $(element).addClass("field-error");
    },
    unhighlight: function (element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function (error, element) {
        return false;

    },
    submitHandler: function (form) {

        var data = new FormData();

        //Form data
        var form_data = $('#useradd').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        //File data
        //        var file_data = $('input[name="image"]')[0].files;
        //        for (var i = 0; i < file_data.length; i++) {
        //            data.append("image", file_data[i]);
        //        }
        $.ajax({
            type: 'post',
            url: 'add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function (response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function () {
                        window.location.replace('/registration');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'setting');
                    //  }, 1500);
                }
            }
        });
    }
});
$('#userUpdate').validate({
    rules:{
       name: {
           required:true
       },
       mobile_no: {
           required:true
       },
       email: {
           required:true
       },
       gender: {
           required:true
       },
       password: {
           minlength:6,
           maxlength:18
       },
       cpassword: {
           required:function(){
               return $('#password').val() != ''
           },
           minlength:6,
           maxlength:18,
           equalTo: "#password"
       }
   },
  highlight: function(element) {
       $(element).addClass("field-error");
   },
   unhighlight: function(element) {
       $(element).removeClass("field-error");
   },
   errorPlacement: function(error, element) {
       return false;

   },
   submitHandler: function (form) {

       var data = new FormData();

       //Form data
       var form_data = $('#userUpdate').serializeArray();
       $.each(form_data, function (key, input) {
           data.append(input.name, input.value);
       });

           $.ajax({
               type: 'post',
               url: '/registration/edit',
               data: data,
               contentType: false,
               cache: false,
               processData:false,

           success: function(response) {
               if(response.status == 'success'){
                    notifictaion(response.status,response.message,'success');
                   setTimeout(function() {
                       window.location.replace('/registration');
                    }, 1500);
               } else {
                       notifictaion(response.status,response.message,'error');
               }
           }
       });
       }
});



