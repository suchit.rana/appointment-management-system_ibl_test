$('#suggestion').validate({
    //    ignore: [],
    rules: {
        title: {
            required: true
        },
        desc: {
            required: true
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#suggestion').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/suggestionbox/',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/suggestionbox');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});

$('#tech').validate({
    rules: {
        technology: {
            required: true
        },
        knowledge: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#tech').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/technicalskill/store',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/technicalskill/');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});



$('#techedit').validate({
      rules: {
        technology: {
            required: true
        },
        knowledge: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#techedit').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        console.log(data)
        $.ajax({
            type: 'post',
            url: '/technicalskill/edit',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/technicalskill/');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});



$('#confirm_candidate').validate({
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#confirm_candidate').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        console.log(data)
        $.ajax({
            type: 'post',
            url: '/interviewschedule/Is_Join',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/interviewschedule/Is_Join');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});

$('#interviewadd') . validate({
        rules: {
        candidate_name :{
            required:true,
            },
        email_id :{
            required:true,
            },
        phone_number :{
            required:true,
            minlength: 10,
            maxlength: 10
            },
        technology :{
            required:true,
            },
        candidate_type :{
            required:true,
            },
        interview_date :{
            required:true,
            },
        interview_time :{
            required:true,
            },
        round :{
            required:true,
            },
    },
    highlight: function (element) {
        $(element).addClass("field-error");
    },
    unhighlight: function (element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function (error, element) {
        return false;
    },
    submitHandler: function (form) {

    var data = new FormData();

    //Form data
    var  form_data = $('#interviewadd').serializeArray();

    $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
    });

    $.ajax({
            type: 'post',
            url: '/interviewschedule/add/',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function ()
                {
                    $("#wait").css("display", "block");
                },
            success: function (response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function () {
                        window.location.replace('/interviewschedule/');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
        $("#wait").css("display", "none");
    }
        });
    }
});

$('#interviewupdate').validate({
    rules: {
				candidate_name :{
					required: true,
				},
				email_id :	{
					required: true,
				},
				phone_number :	{
					required: true,
                    minlength: 10,
                    maxlength: 10
				},
                technology :	{
					required: true,
				},
                candidate_type :	{
					required: true,
				},
                interview_date :	{
					required: true,
				},
				interview_time :	{
					required: true,
				},
				round :	{
					required: true,
				},
			},
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#interviewupdate').serializeArray();

        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/interviewschedule/add/',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/interviewschedule/');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});





$('#details_adds').validate({
    //    ignore: [],
    rules: {
        name: {
            required: true
        },
        email: {
            required: true
        },
        mobile_no: {
            required: true
        },
        department_id: {
            required: true
        },
        dob: {
            required: true
        },
        gender: {
            required: true
        },
        address: {
            required: true
        },
        known_technology: {
            required: true
        },
        aadhar_card: {
            required: true,
            minlength: 12,
            maxlength: 12
        },
        training_months: {
            required: true
        },
        training_stipend: {
            required: true
        },
        joining_date: {
            required: true
        },
        bond_years: {
            required: true
        },
        bond_end_date: {
            required: true
        },
        base_salary: {
            required: true
        },
        per_day_salary: {
            required: true
        },
        ac_name: {
            required: true
        },
        ac_number: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#details_adds').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/candidatedetail/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/candidatedetail');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});




$('#details_update').validate({
    //    ignore: [],
    rules: {
        department_id: {
            required: true
        },
        dob: {
            required: true
        },
        email_id: {
            required: true,
            email: true,
            maxlength: 30,
            validate_email: true
        },
        phone_number: {
            required: true,
            minlength: 10,
            maxlength: 10,
        },
        gender: {
            required: true
        },
        address: {
            required: true
        },
        known_technology: {
            required: true
        },
        aadhar_card: {
            required: true,
            minlength: 12,
            maxlength: 12
        },
        training_months: {
            required: true
        },
        training_stipend: {
            required: true
        },
        joining_date: {
            required: true
        },
        bond_years: {
            required: true
        },
        bond_end_date: {
            required: true
        },
        base_salary: {
            required: true
        },
        per_day_salary: {
            required: true
        },
        ac_name: {
            required: true
        },
        ac_number: {
            required: true
        }
    },
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        console.log(data)
        //Form data
        var form_data = $('#details_update').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/candidatedetail/edit',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/candidatedetail');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});



$('#bondrenew').validate({
    //    ignore: [],
    rules: {
        department_id: {
            required: true
        },
        joining_date: {
            required: true
        },
        bond_years: {
            required: true
        },
        bond_end_date: {
            required: true
        },
        base_salary: {
            required: true
        },
        per_day_salary: {
            required: true
        },
    },
    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        console.log(data)
        //Form data
        var form_data = $('#bondrenew').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            type: 'post',
            url: '/candidatedetail/renew',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                if (response.status == 'success') {
                    notifictaion(response.status, response.message, 'success');
                    setTimeout(function() {
                        window.location.replace('/candidatedetail');
                    }, 1500);
                } else {
                    notifictaion(response.status, response.message, 'error');
                }
            }
        });
    }
});



function notifictaion(title, message, type) {
    new PNotify({
        title: title,
        text: message,
        type: type,
        cornerclass: 'ui-pnotify-sharp'
    });
}

function deleterecord(id, path) {
    swal({
            title: "Are you sure to delete?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'get',
                    url: path,
                    data: { id: id },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('#' + id).remove();
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            // setTimeout(function() {
                            //      location.reload();
                            // }, 1500);

                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted", {
                    icon: "error",
                });
            }
        });
}


function terminate(id, path) {
    swal({
            title: "Are you sure to Terminate?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'get',
                    url: path,
                    data: { id: id },
                    success: function(data) {
                        if (data.status == 'success') {
                            $('#' + id).remove();
                            swal("Terminated!", data.message, {
                                icon: "success",
                            });
                             setTimeout(function() {
                                  location.reload();
                             }, 1300);

                        } else {
                            swal("Terminated Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Terminated Aborted", {
                    icon: "error",
                });
            }
        });
}

// function deleterecord(id, path) {

//     swal({
//             title: "Are you sure Want to Delete.?",
//             text: "",
//             icon: "warning",
//             buttons: true,
//             dangerMode: true,
//         })
//         .then((willDelete) => {
//             if (willDelete) {
//                 $.ajax({
//                     type: 'post',
//                     url: path,
//                     data: { id: id },
//                     success: function(result) {
//                         var data = JSON.parse(result);
//                         if (data.status == 'success') {
//                             swal("Deleted!", data.message, {
//                                 icon: "success",
//                             });
//                             // setTimeout(function() {
//                             //      location.reload();
//                             // }, 1500);

//                         } else {
//                             swal("Delete Aborted", data.message, {
//                                 icon: "error",
//                             });
//                         }

//                     }
//                 });
//             } else {
//                 swal("Relax Delete Aborted", {
//                     icon: "error",
//                 });
//             }
//         });
// }


$('#log').change(function() {
    var data = $('option:selected', this).val();
    $.ajax({
        type: 'post',
        url: Admin_url + 'auth/sessionupdate',
        data: { id: data },
        success: function(result) {
            var data = JSON.parse(result);
            if (data.status == 'success') {
                notifictaion(data.status, data.message, 'success');
                setTimeout(function() {
                    window.location.replace(Admin_url + 'auth/login');
                }, 1500);
                /* window.location.replace( Admin_url + 'auth/login');*/
            }
        }
    });
});



$('.onlynumber').on('keydown keypress keyup paste input', function() {
    while (($(this).val().split(".").length - 1) > 1) {

        $(this).val($(this).val().slice(0, -1));

        if (($(this).val().split(".").length - 1) > 1) {
            continue;
        } else {
            return false;
        }
    }

    $(this).val($(this).val().replace(/[^0-9.]/g, ''));

    var int_num_allow = 10;
    var float_num_allow = 2;
    var iof = $(this).val().indexOf(".");
    if (iof != -1) {
        if ($(this).val().substring(0, iof).length > int_num_allow) {
            $(this).val('');

            $(this).attr('placeholder', 'invalid number');
        }

        $(this).val($(this).val().substring(0, iof + float_num_allow + 1));
    } else {
        $(this).val($(this).val().substring(0, int_num_allow));
    }
    return true;
});


function deletesurname(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'setting/deletesurname',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'setting/surname');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}



function deleteadvertise(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'setting/deleteadvertise',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'setting/advertise');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}

function deletesponsor(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'sponsor/deletesponsor',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'sponsor');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}

/*Delete Standard*/
function deletestandard(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'setting/deletestandard',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'standard');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}
/*End of standard Delete*/


function deletedonor(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'donor/deletedonor',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'donor');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}

function deleteexpense(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'expense/deleteexpense',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'expense');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}

function deleteyear(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'setting/deleteyear',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'setting/year');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}




function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
        }

        console.log(reader);
        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function() {
    readURL(this);
    $('#editprofile').submit();
});

$('#editprofile').validate({
    rules: {},

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {
        var data = new FormData();

        //Form data
        var form_data = $('#editprofile').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="image"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            data.append("image", file_data[i]);

        }
        $.ajax({
            type: 'post',
            url: Admin_url + 'user/profileupload',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'user/profile');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    /*One.helpers('notify', {type: 'danger', icon: 'fa fa-check mr-1', message:'Not Updated'});*/
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'user/profile');
                    //  }, 1500);
                }
            }
        });
    }
})




$('#profileedit').validate({

    rules: {
        name: {
            required: true
        },
        mobile: {
            required: true,
            minlength: 10,
            maxlength: 10,
        },

    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#profileedit').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        /* var file_data = $('input[name="image"]')[0].files;
         for (var i = 0; i < file_data.length; i++) {
             data.append("image", file_data[i]);
         }*/

        $.ajax({
            type: 'post',
            url: Admin_url + 'user/adminprofile',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'user/profile');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    /*One.helpers('notify', {type: 'danger', icon: 'fa fa-check mr-1', message:'Not Updated'});*/
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'user/profile');
                    //  }, 1500);
                }
            }
        });
    }
});

/*
$('#year').validate({
     rules:{
        year:{
            required:true
        }
    },

   highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function (form) {

            var data = new FormData();
            var form_data = $('#year').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });
             $.ajax({
                   type: 'post',
                    url: Admin_url + 'setting/year',
                    data: data,
                    contentType: false,
                    cache: false,
                    processData:false,

                success: function(response) {

                    var data =  jQuery.parseJSON(response);
                    if(data.status == 'success'){
                        $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                        notifictaion(data.status,data.message,'success');
                        setTimeout(function() {
                                window.location.replace(Admin_url + 'setting/year');
                             }, 1500);
                    } else {
                        notifictaion(data.status,data.message,'error');
                    }
                }
            });
        }
});*/
$('#expense').validate({
    rules: {
        title: {
            required: true
        },
        date: {
            required: true
        },
        amount: {
            required: true
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#expense').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'expense/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'expense');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});



$('#function').validate({
    rules: {
        title: {
            required: true
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#function').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'functions/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'functions');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});


$('#donor').validate({
    rules: {
        member: {
            required: true
        },
        amount: {
            required: true
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#donor').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'donor/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'donor');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});

$('#workallocation').validate({
    rules: {
        "member[]": {
            required: true
        },
        "work": {
            required: true
        }
    },

    highlight: function(element) {
        console.log(element);
        $(element).addClass("field-error");
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#workallocation').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'work/allocation',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'work/allocation');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});


$('#sponsor').validate({
    rules: {
        member: {
            required: true
        },
        sponsortype: {
            required: true
        },
        amount: {
            required: true
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#sponsor').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'sponsor/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'sponsor');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});

$('#work').validate({
    rules: {
        title: {
            required: true
        },
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#work').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'work/add',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'work');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});



$('#role').validate({
    rules: {
        role: {
            required: true,
        },
        permission: {
            required: true,
        }

    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();
        var form_data = $('#role').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'setting/role',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {

                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'setting/role');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                }
            }
        });
    }
});

function deleterole(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'setting/deleterole',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'setting/role');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}



function deleteworkallocation(id) {
    swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: Admin_url + 'work/deleteworkallocation',
                    data: { id: id },
                    success: function(result) {
                        var data = JSON.parse(result);
                        if (data.status == 'success') {
                            $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                            swal("Deleted!", data.message, {
                                icon: "success",
                            });
                            setTimeout(function() {
                                window.location.replace(Admin_url + 'work/allocation');
                            }, 1500);

                            $('#' + id).remove();
                        } else {
                            swal("Delete Aborted", data.message, {
                                icon: "error",
                            });
                        }

                    }
                });
            } else {
                swal("Relax Delete Aborted");
            }
        });
}



$('.confirmation-callback').confirmation({

    onConfirm: function() {
        var dat = $(this)[0].id.split("/");
        var urlss = window.location.href;
        $.ajax({
            type: 'post',
            url: Admin_url + dat[1] + '/' + dat[2],
            data: { id: dat[0] },
            success: function(result) {
                var data = JSON.parse(result);
                if (data.status == 'success') {
                    swal("Deleted!", data.message, {
                        icon: "success",
                    });
                    setTimeout(function() {
                        window.location.replace(urlss);
                    }, 1500);
                    $('#' + id).remove();
                } else {
                    swal("Delete Aborted", data.message, {
                        icon: "error",
                    });
                }
            }
        });
    },
    onCancel: function() {
        swal("Delete Aborted", "Relax Is Not Delete", {
            icon: "error",
        });
    }
});

$('#sponsorlogo').validate({

    rules: {
        sponsor: {
            required: true,
        },
        /* image: {
             required:true,
         },*/
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#sponsorlogo').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="image"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            data.append("image", file_data[i]);
        }
        $.ajax({
            type: 'post',
            url: Admin_url + 'sponsor/logo',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'sponsor/logo');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'sponsor/logo');
                    //  }, 1500);
                }
            }
        });
    }
});
$('#memberads').validate({

    rules: {
        titles: {
            required: true,
            maxlength: 50,
        },
        member: {
            required: true,
        },
        category: {
            required: true,
        },
        address: {
            required: true,
        },
        /* image: {
             required:true,
         },*/
        mobile: {
            required: true,
            minlength: 10,
            maxlength: 10,
        }

    },

    highlight: function(element) {
        $(element).addClass("field-error");
        $(element).find('.select2').addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#memberads').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="image"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            data.append("image", file_data[i]);
        }
        $.ajax({
            type: 'post',
            url: Admin_url + 'member/ads',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);

                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'member/ads');
                    }, 1500);
                } else if (data.status == 'error') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'member/ads');
                    //  }, 1500);
                }
            }
        });
    }
});

$('#slider').validate({

    rules: {
        title: {
            required: true,
        },
        /* image: {
             required:true,
         },*/
        position: {
            required: true,
            maxlength: 2,
        }

    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {

        var data = new FormData();

        //Form data
        var form_data = $('#slider').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="image"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            data.append("image", file_data[i]);
        }
        $.ajax({
            type: 'post',
            url: Admin_url + 'setting/slider',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'setting/slider');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'setting/slider');
                    //  }, 1500);
                }
            }
        });
    }
});








$('#gallery').validate({

    rules: {
        title: {
            required: true,
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {
        var formData = new FormData();
        var fileInput = $('#image')[0];
        var form_data = $('#gallery').serializeArray();
        $.each(form_data, function(key, input) {
            formData.append(input.name, input.value);
        });
        if (fileInput.files.length > 0) {
            $.each(fileInput.files, function(k, file) {
                formData.append('images[]', file);
            });
        }
        $.ajax({
            type: 'post',
            url: Admin_url + 'gallery/add',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'gallery');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'gallery');
                    //  }, 1500);
                }
            }
        });
    }
});


$('#category').validate({

    rules: {
        ctitle: {
            required: true,
        }
    },

    highlight: function(element) {
        $(element).addClass("field-error");
    },
    unhighlight: function(element) {
        $(element).removeClass("field-error");
    },
    errorPlacement: function(error, element) {
        return false;

    },
    submitHandler: function(form) {
        var data = new FormData();
        var form_data = $('#category').serializeArray();
        $.each(form_data, function(key, input) {
            data.append(input.name, input.value);
        });
        $.ajax({
            type: 'post',
            url: Admin_url + 'category',
            data: data,
            contentType: false,
            cache: false,
            processData: false,

            success: function(response) {
                var data = jQuery.parseJSON(response);
                if (data.status == 'success') {
                    $("input[type=text], textarea,input[type=email],input[type=password],input[type=number],input[type=date]").val("");
                    notifictaion(data.status, data.message, 'success');
                    setTimeout(function() {
                        window.location.replace(Admin_url + 'category');
                    }, 1500);
                } else {
                    notifictaion(data.status, data.message, 'error');
                    // setTimeout(function() {
                    //     window.location.replace(Admin_url + 'category');
                    //  }, 1500);
                }
            }
        });
    }
});


$(document).ready(function() {});

$(document).ready(function() {
    $("#surname").on("change", function() {
        var value = $(this).val().toLowerCase();
        $(".datatable-tabletools tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

$(document).ready(function() {
    $("#std").on("change", function() {
        var value = $(this).val().toLowerCase();
        $(".tabletools tbody tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

$('input[type=radio][name=resource]').change(function() {
    if (this.value == 'image') {
        $('.image').removeClass('radiohide');
        $('.video').addClass('radiohide');
    } else if (this.value == 'video') {
        $('.video').removeClass('radiohide');
        $('.image').addClass('radiohide');
    }
});
/*$(document).ready(function(){
  $(".resourceradio").on("click", function() {
    var value = $(this).val();
    console.log(value);

    $(".tabletools tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

*/