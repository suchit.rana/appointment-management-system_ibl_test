from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns =[
        path("", views.index),
        path("logout", views.logout ,name='logout'),
        # path("changepass", views.changepass ,name='changepass'),
        # path('login/setWork', views.setWork, name='setWork'),
    ]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)