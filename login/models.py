from django.db import models
from django.utils import timezone
# from registration.models  import users
from django.db import models

# Create your models here.


class LoginSoftDelete(models.Manager):
    def get_queryset(self):
        return super(LoginSoftDelete, self).get_queryset().filter(deleted_at__isnull=True).order_by('-id')


class Login(models.Model):
    user_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=200, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = LoginSoftDelete()


    def delete(self):
        self.deleted_at = timezone.now()
        self.save()
