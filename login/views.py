from django.conf import settings
from django.shortcuts import render,redirect
from registration.models import users as User
from django.contrib.auth.hashers import make_password,check_password
from django.http import JsonResponse
from datetime import datetime
import random
import re


# Create your views here.

def index(request):
    if (request.session.has_key('User')):
        return redirect("/dashboard")
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if email == "" or password == "":
            context = {'error': True, 'message': 'Email Or Password Are Required ..!'}
            return render(request, 'login.html', context)
        if re.search(regex, email):
            post = User.objects.filter(email=email).first()
            if post:
                if check_password(password, post.password):
                    session_id = random.randint(100000000000, 999999999999)
                    userdata = {'session_id': session_id, 'id': post.id, 'name': post.name,
                                'type_id': post.type_id,
                                 'email': post.email}
                    request.session['User'] = userdata

                    now = datetime.now()
                    return redirect("/dashboard")
                else:
                    context = {'error': True, 'message': 'Email Or Password Wrong ..! Please Try Again.'}
                    return render(request, 'login.html', context)
            else:
                context = {'error': True, 'message': 'Email Or Password Wrong ..! Please Try Again.'}
                return render(request, 'login.html', context)
        else:
            context = {'error': True, 'message': 'Please Enter Valid Email ..!'}
            return render(request, 'login.html', context)

    return render(request, 'login.html')


def logout(request):
    if (request.session.has_key('User')):
        user_id = request.session['User']['id']
        session_id = request.session['User']['session_id']
        request.session.modified = True
        del request.session['User']
        # del request.session['current_department']
    return redirect("/")